﻿using MLW.Demos.Azure.Functions.EventLogging.Config;
using Xunit;

namespace MLW.Demos.Azure.Functions.EventLogging.Tests.ConfigTests
{
    public class AppSettingsHelperTests
    {
        [Fact]
        public void GetConfig_ShouldReturnConfig()
        {
            var actual = AppSettingsHelper.GetConfig();

            Assert.NotNull(actual);
        }

        [Fact]
        public void GetEventHubConnectionString_ShouldReturnValue()
        {
            var actual = AppSettingsHelper.GetEventHubConnectionString();

            Assert.False(string.IsNullOrWhiteSpace(actual));
        }

        [Fact]
        public void GetEventLogsEntityPathName_ShouldReturnValue()
        {
            var actual = AppSettingsHelper.GetEventLogsEntityPathName();

            Assert.False(string.IsNullOrWhiteSpace(actual));
        }

        [Fact]
        public void GetLoggingServiceFunctionUrl_ShouldReturnValue()
        {
            var actual = AppSettingsHelper.GetLoggingServiceFunctionUrl();

            Assert.False(string.IsNullOrWhiteSpace(actual));
        }

        [Fact]
        public void GetServiceBusConnectionString_ShouldReturnValue()
        {
            var actual = AppSettingsHelper.GetServiceBusConnectionString();

            Assert.False(string.IsNullOrWhiteSpace(actual));
        }

        [Fact]
        public void GetExecutingAssemblyLocation_ShouldReturnValue()
        {
            var actual = AppSettingsHelper.GetExecutingAssemblyLocation();

            Assert.False(string.IsNullOrWhiteSpace(actual));
        }
    }
}
