﻿using System;
using MLW.Demos.Azure.Functions.EventLogging.Config;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using MLW.Demos.Azure.Functions.EventLogging.Publishers;
using Xunit;

namespace MLW.Demos.Azure.Functions.EventLogging.Tests.PublisherTests
{
    public class EventLogPublisherTests
    {
        [Fact]
        public void Publish_ShouldProcessSuccessfully()
        {
            var testPublisher = new EventLogTopicPublisher(
                AppSettingsHelper.GetEventHubConnectionString(), 
                AppSettingsHelper.GetEventLogsEntityPathName());

            // publish test message for each log level
            foreach (var item in Enum.GetValues(typeof(LogLevel)))
            {
                var testMessage = new LogMessage
                {
                    LogLevel = (LogLevel) item,
                    MessageText = $"This is a sample [{(LogLevel) item}] alert " +
                                  "using the EventPublisher.",
                    SenderId = GetType().Name
                };

                testPublisher.Publish(testMessage);
            }
        }
    }
}
