﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace MLW.Demos.Azure.Functions.EventLogging.Config
{
    public static class AppSettingsHelper
    {
        private static IConfigurationRoot _config;

        public static string GetExecutingAssemblyLocation()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        public static void LoadConfig()
        {
            _config = new ConfigurationBuilder()
                .SetBasePath(GetExecutingAssemblyLocation())
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .Build();
        }

        public static IConfigurationRoot GetConfig()
        {
            if (_config == null) LoadConfig();
            return _config;
        }

        public static void SetConfig(IConfigurationRoot config)
        {
            _config = config;
        }

        public static string GetLoggingServiceFunctionUrl()
        {
            return GetConfig()["LoggingServiceFunctionUrl"];
        }

        public static string GetEventHubConnectionString()
        {
            return GetConfig()["EventHubConnectionString"];
        }

        public static string GetEventLogsEntityPathName()
        {
            return GetConfig()["EventLogsEntityPathName"];
        }

        public static string GetServiceBusConnectionString()
        {
            return GetConfig()["ServiceBusConnectionString"];
        }

        public static string GetWorkItemsQueueName()
        {
            return GetConfig()["WorkItemsQueueName"];
        }

        public static string GetEventLogChannelWebhookUrl()
        {
            return GetConfig()["EventLogsChannelWebhookUrl"];
        }

        public static string GetWorkItemChannelWebhookUrl()
        {
            return GetConfig()["WorkItemsChannelWebhookUrl"];
        }
    }
}
