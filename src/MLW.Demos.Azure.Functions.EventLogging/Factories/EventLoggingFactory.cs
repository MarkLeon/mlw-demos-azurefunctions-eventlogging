﻿using MLW.Demos.Azure.Functions.EventLogging.Config;
using MLW.Demos.Azure.Functions.EventLogging.Publishers;

namespace MLW.Demos.Azure.Functions.EventLogging.Factories
{
    public static class EventLoggingFactory
    {
        public static EventLogTopicPublisher GetNewEventLogTopicPublisher()
        {
            return new EventLogTopicPublisher(
                AppSettingsHelper.GetEventHubConnectionString(),
                AppSettingsHelper.GetEventLogsEntityPathName());
        }

        public static HttpEventPublisher GetNewHttpEventLogPublisher()
        {
            return new HttpEventPublisher(
                AppSettingsHelper.GetLoggingServiceFunctionUrl());
        }

        public static WorkItemQueuePublisher GetNewWorkItemQueuePublisher()
        {
            return new WorkItemQueuePublisher(
                AppSettingsHelper.GetServiceBusConnectionString(),
                AppSettingsHelper.GetWorkItemsQueueName());
        }

        public static ChannelPublisher GetNewEventLogChannelPublisher()
        {
            return new ChannelPublisher(
                AppSettingsHelper.GetEventLogChannelWebhookUrl());
        }

        public static ChannelPublisher GetNewWorkItemChannelPublisher()
        {
            return new ChannelPublisher(
                AppSettingsHelper.GetWorkItemChannelWebhookUrl());
        }
    }
}
