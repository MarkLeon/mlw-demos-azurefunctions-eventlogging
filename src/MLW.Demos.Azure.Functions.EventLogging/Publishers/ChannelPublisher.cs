﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using MLW.Demos.Azure.Functions.EventLogging.Helpers;
using Newtonsoft.Json;

namespace MLW.Demos.Azure.Functions.EventLogging.Publishers
{
    public class ChannelPublisher : ILogMessagePublisher
    {
        private readonly string _webhookUrl;
        private readonly HttpClient _httpClient;

        public ChannelPublisher(string webhookUrl)
        {
            _webhookUrl = webhookUrl;
            // init http client
            _httpClient = new HttpClient();
        }

        ~ChannelPublisher()
        {
            // cleanup
            _httpClient?.Dispose();
        }

        public async Task HttpPostMessage(string message)
        {
            // format message for slack expectations
            var messageObject = new { text = message };

            // encode message
            var encodedMessage = new StringContent(
                JsonConvert.SerializeObject(messageObject),
                Encoding.UTF8,
                "application/json");

            // post encoded message to slack
            using (var httpResponse = await _httpClient.PostAsync(_webhookUrl, encodedMessage))
            {
                // process response
                if (!httpResponse.IsSuccessStatusCode)
                {
                    // process unsuccessful status
                    var errorMessage =
                        "SlackPublisher.PublishMessageAsync(): A unsuccessful status code was returned from Slack API. \n" +
                        $"response.StatusCode = {httpResponse.StatusCode}; \n" +
                        $"response.ReasonPhrase = {httpResponse.ReasonPhrase}; \n" +
                        $"response.RequestMessage = {httpResponse.RequestMessage}; \n" +
                        $"response.Content.ReadAsStringAsync() = {httpResponse.Content.ReadAsStringAsync()}; \n";

                    throw new Exception(errorMessage);
                }
            }
        }

        public async Task PublishAsync(LogMessage message)
        {
            await HttpPostMessage(message.ToChannelMessage());
        }

        public void Publish(LogMessage message)
        {
            Task.Run(async () => await PublishAsync(message));
        }
    }
}
