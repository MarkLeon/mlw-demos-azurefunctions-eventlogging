﻿using System.Threading.Tasks;
using Microsoft.Azure.ServiceBus;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using MLW.Demos.Azure.Functions.EventLogging.Helpers;

namespace MLW.Demos.Azure.Functions.EventLogging.Publishers
{
    public class WorkItemQueuePublisher : ILogMessagePublisher
    {
        private readonly QueueClient _queueClient;

        public WorkItemQueuePublisher(string connectionString, string queueName)
        {
            // init queue client
            _queueClient = new QueueClient(connectionString, queueName);
        }

        ~WorkItemQueuePublisher()
        {
            if (_queueClient == null) return;
            // close queue client
            Task.Run(async () => await _queueClient.CloseAsync());
        }

        public async Task PublishAsync(LogMessage message)
        {
            // publish message to queue
            await _queueClient.SendAsync(new Message(message.ToJsonEncoded()));
        }

        public void Publish(LogMessage message)
        {
            Task.Run(async () => await PublishAsync(message));
        }
    }
}
