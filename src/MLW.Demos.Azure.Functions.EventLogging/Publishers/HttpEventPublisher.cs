﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using Newtonsoft.Json;

namespace MLW.Demos.Azure.Functions.EventLogging.Publishers
{
    public class HttpEventPublisher : ILogMessagePublisher
    {
        private readonly string _uri;
        private readonly HttpClient _httpClient;

        public HttpEventPublisher(string uri)
        {
            _uri = uri;
            _httpClient = new HttpClient();
        }

        ~HttpEventPublisher()
        {
            _httpClient?.Dispose();
        }

        public async Task HttpPostMessage(LogMessage message)
        {
            // encode message
            var encodedMessage = new StringContent(
                JsonConvert.SerializeObject(message),
                Encoding.UTF8,
                "application/json");

            // post encoded message to web service
            using (var httpResponse = await _httpClient.PostAsync(_uri, encodedMessage))
            {
                // process response
                if (!httpResponse.IsSuccessStatusCode)
                {
                    // process unsuccessful status
                    var errorMessage =
                        "SlackPublisher.PublishMessageAsync(): A unsuccessful status code was returned from Slack API. \n" +
                        $"response.StatusCode = {httpResponse.StatusCode}; \n" +
                        $"response.ReasonPhrase = {httpResponse.ReasonPhrase}; \n" +
                        $"response.RequestMessage = {httpResponse.RequestMessage}; \n" +
                        $"response.Content.ReadAsStringAsync() = {httpResponse.Content.ReadAsStringAsync()}; \n";

                    throw new Exception(errorMessage);
                }
            }
        }

        public async Task PublishAsync(LogMessage message)
        {
            await HttpPostMessage(message);
        }

        public void Publish(LogMessage message)
        {
            Task.Run(async () => await PublishAsync(message));
        }
    }
}
