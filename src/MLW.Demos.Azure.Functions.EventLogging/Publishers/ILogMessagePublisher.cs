﻿using System.Threading.Tasks;
using MLW.Demos.Azure.Functions.EventLogging.Entities;

namespace MLW.Demos.Azure.Functions.EventLogging.Publishers
{
    public interface ILogMessagePublisher
    {
        Task PublishAsync(LogMessage message);
        void Publish(LogMessage message);
    }
}
