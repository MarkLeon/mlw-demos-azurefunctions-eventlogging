﻿using System.Threading.Tasks;
using Microsoft.Azure.EventHubs;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using MLW.Demos.Azure.Functions.EventLogging.Helpers;

namespace MLW.Demos.Azure.Functions.EventLogging.Publishers
{
    public class EventLogTopicPublisher : ILogMessagePublisher
    {
        private readonly EventHubClient _eventHubClient;

        public EventLogTopicPublisher(string connectionString, string entityPathName)
        {
            // init event hub client
            var connectionStringBuilder = new EventHubsConnectionStringBuilder(connectionString) { EntityPath = entityPathName };
            _eventHubClient = EventHubClient.CreateFromConnectionString(connectionStringBuilder.ToString());
        }

        ~EventLogTopicPublisher()
        {
            if (_eventHubClient == null) return;
            // close event hub client
            Task.Run(async () => await _eventHubClient.CloseAsync());
        }

        public async Task PublishAsync(LogMessage message)
        {
            // publish message to event hub
            await _eventHubClient.SendAsync(new EventData(message.ToJsonEncoded()));
        }

        public void Publish(LogMessage message)
        {
            Task.Run(async () => await PublishAsync(message));
        }
    }
}
