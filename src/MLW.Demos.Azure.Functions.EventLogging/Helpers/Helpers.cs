﻿using System.Text;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using Newtonsoft.Json;

namespace MLW.Demos.Azure.Functions.EventLogging.Helpers
{
    public static class Helpers
    {
        public static bool IsValid(this LogMessage message)
        {
            return message != null &&
                   !string.IsNullOrWhiteSpace(message.SenderId) && 
                   !string.IsNullOrWhiteSpace(message.MessageText);
        }

        public static bool ShouldPublishAsWorkItem(this LogMessage message)
        {
            return message.LogLevel >= LogLevel.Error;
        }

        public static string ToJson(this LogMessage message)
        {
            return JsonConvert.SerializeObject(message);
        }

        public static byte[] EncodeString(string message)
        {
            return Encoding.UTF8.GetBytes(message);
        }

        public static byte[] ToJsonEncoded(this LogMessage message)
        {
            var json = message.ToJson();
            var encoded = EncodeString(json);
            return encoded;
        }

        public static string ToChannelMessage(this LogMessage message)
        {
            return $"*{message.LogLevel.ToString().ToUpper()}*: {message.MessageText}  \n" +
                $"Message ID = {message.MessageId}  \n" +
                $"Message Client ID = {message.SenderId}  \n" +
                $"Message Create Date/Time = {message.CreateDateTime}  \n" +
                $"Message Updated Date/Time = {message.UpdatedDateTime}  \n" +
                $"Message Path = {message.MessagePath}  \n";
        }
    }
}
