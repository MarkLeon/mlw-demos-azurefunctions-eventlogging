﻿using System;

namespace MLW.Demos.Azure.Functions.EventLogging.Entities
{
    public class LogMessage
    {
        // typical log message data
        public LogLevel LogLevel { get; set; } = LogLevel.Info;
        public string MessageText { get; set; } = "";

        // messaging pipeline and transactional data
        public Guid MessageId { get; set; } = Guid.NewGuid();
        public string SenderId { get; set; } = "";
        public string MessagePath { get; set; } = "";
        public DateTime CreateDateTime { get; set; } = DateTime.Now;
        public DateTime UpdatedDateTime { get; set; } = DateTime.Now;
    }
}
