﻿namespace MLW.Demos.Azure.Functions.EventLogging.Entities
{
    public enum LogLevel
    {
        Debug = 1,
        Info = 2,
        Warn = 3,
        Error = 4,
        Fatal = 5
    }
}
