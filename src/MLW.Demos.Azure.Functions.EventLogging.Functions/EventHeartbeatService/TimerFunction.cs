using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using MLW.Demos.Azure.Functions.EventLogging.Factories;
using MLW.Demos.Azure.Functions.EventLogging.Functions.Shared;
using LogLevel = MLW.Demos.Azure.Functions.EventLogging.Entities.LogLevel;

namespace MLW.Demos.Azure.Functions.EventLogging.Functions.EventHeartbeatService
{
    public static class TimerFunction
    {
        [FunctionName("EventHeartbeatFunction")]
        public static void Run(
            [TimerTrigger("*/15 * * * * *")]
            TimerInfo timer, 
            ILogger log, 
            ExecutionContext context)
        {
            // init
            FunctionContextHelper.Init(log, context);
            FunctionContextHelper.TrackStart();

            // publish heartbeat event message

            var eventPublisher = EventLoggingFactory.GetNewHttpEventLogPublisher();

            var heartBeatMessage = new LogMessage()
            {
                LogLevel = GetRandomLogLevel(),
                MessageText = $"This is a heartbeat event message from the EventHeartbeatService @ {DateTime.Now}",
                SenderId = "EventHeartbeatService",
                MessagePath = context.FunctionName
            };

            eventPublisher.Publish(heartBeatMessage);

            // end
            FunctionContextHelper.TrackEnd();
        }

        public static LogLevel GetRandomLogLevel()
        {
            var rnd = new Random(DateTime.Now.Millisecond);
            return (LogLevel)rnd.Next((int) LogLevel.Debug, (int) LogLevel.Fatal);
        }
    }
}
