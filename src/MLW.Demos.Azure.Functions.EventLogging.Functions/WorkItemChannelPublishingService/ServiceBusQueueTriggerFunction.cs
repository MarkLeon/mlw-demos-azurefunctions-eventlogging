using System;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using MLW.Demos.Azure.Functions.EventLogging.Factories;
using MLW.Demos.Azure.Functions.EventLogging.Functions.Shared;
using Newtonsoft.Json;

namespace MLW.Demos.Azure.Functions.EventLogging.Functions.WorkItemChannelPublishingService
{
    public static class ServiceBusQueueTriggerFunction
    {
        [FunctionName("WorkItemChannelPublisherFunction")]
        public static void Run(
            [ServiceBusTrigger("%WorkItemsQueueName%", Connection = "ServiceBusConnectionString")]
            string workQueueItem, 
            ILogger log,
            ExecutionContext context)
        {
            // init
            FunctionContextHelper.Init(log, context);
            FunctionContextHelper.TrackStart();

            // parse message
            var channelPublisher = EventLoggingFactory.GetNewWorkItemChannelPublisher();
            var eventLog = JsonConvert.DeserializeObject<LogMessage>(workQueueItem);

            // append service path info
            eventLog.MessagePath += $" -> {context.FunctionName}";
            eventLog.UpdatedDateTime = DateTime.Now;

            // publish event
            channelPublisher.Publish(eventLog);

            // end
            FunctionContextHelper.TrackEnd();
        }
    }
}
