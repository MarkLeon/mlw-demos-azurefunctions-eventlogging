﻿using Microsoft.ApplicationInsights;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MLW.Demos.Azure.Functions.EventLogging.Config;

namespace MLW.Demos.Azure.Functions.EventLogging.Functions.Shared
{
    public static class FunctionContextHelper
    {
        public static ILogger Logger { get; set; }
        public static ExecutionContext Context { get; set; }
        public static TelemetryClient TelemetryClient { get; set; }

        public static void Init(ILogger functionLogger, ExecutionContext functionContext)
        {
            functionLogger.LogInformation("Initializing FunctionContextHelper...");

            Logger = functionLogger;
            Context = functionContext;

            TelemetryClient = new TelemetryClient();

            LoadConfig();
        }

        public static void LoadConfig()
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Context.FunctionAppDirectory)
                .AddJsonFile("local.settings.json", optional: true, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            AppSettingsHelper.SetConfig(config);
        }

        public static void TrackStart()
        {
            Logger.LogInformation($"{Context.FunctionName.ToUpper()}: Tracking start of function...");
        }

        public static void TrackEnd()
        {
            Logger.LogInformation($"{Context.FunctionName.ToUpper()}: Tracking end of function...");
        }
    }
}
