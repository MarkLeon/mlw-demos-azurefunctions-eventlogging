using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using MLW.Demos.Azure.Functions.EventLogging.Factories;
using MLW.Demos.Azure.Functions.EventLogging.Functions.Shared;
using MLW.Demos.Azure.Functions.EventLogging.Helpers;
using Newtonsoft.Json;

namespace MLW.Demos.Azure.Functions.EventLogging.Functions.LoggingService
{
    public static class HttpTriggerFunction
    {
        [FunctionName("EventLoggingFunction")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)]
            HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            // init

            FunctionContextHelper.Init(log, context);
            FunctionContextHelper.TrackStart();

            // process http request

            ActionResult actionResult;

            try
            {
                // parse input
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                var eventLog = JsonConvert.DeserializeObject<LogMessage>(requestBody);

                // validate and process input
                if (eventLog.IsValid())
                {
                    var successMessage = $"Valid message received from {eventLog.SenderId}: [{eventLog.LogLevel}] {eventLog.MessageText}";
                    log.LogInformation(successMessage);

                    // append service path info
                    eventLog.MessagePath += $" -> {context.FunctionName}";
                    eventLog.UpdatedDateTime = DateTime.Now;

                    // publish event
                    var eventPublisher = EventLoggingFactory.GetNewEventLogTopicPublisher();
                    await eventPublisher.PublishAsync(eventLog);

                    actionResult = new OkObjectResult(successMessage);
                }
                else
                {
                    // invalid input
                    var errorMessage = "Error! LogLevel, MessageText, and SenderId are all required.";
                    log.LogError(errorMessage);

                    actionResult = new BadRequestObjectResult(errorMessage);
                }

            }
            catch (Exception ex)
            {
                var exMessage = $"Exception encountered: {ex.Message}";
                log.LogError(exMessage);
                actionResult = new BadRequestObjectResult(exMessage);
            }

            // end

            FunctionContextHelper.TrackEnd();

            return actionResult;
        }
    }
}
