using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Azure.EventHubs;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using MLW.Demos.Azure.Functions.EventLogging.Entities;
using MLW.Demos.Azure.Functions.EventLogging.Factories;
using MLW.Demos.Azure.Functions.EventLogging.Functions.Shared;
using MLW.Demos.Azure.Functions.EventLogging.Helpers;
using Newtonsoft.Json;

namespace MLW.Demos.Azure.Functions.EventLogging.Functions.WorkItemRoutingService
{
    public static class EventHubTriggerFunction
    {
        [FunctionName("WorkItemRoutingFunction")]
        public static async Task Run(
            [EventHubTrigger(
                "%EventLogsEntityPathName%",
                Connection = "EventHubConnectionString",
                ConsumerGroup = "%WorkItemChannelPublisherConsumer%")]
            EventData[] events,
            ILogger log, 
            ExecutionContext context)
        {
            // init
            FunctionContextHelper.Init(log, context);
            FunctionContextHelper.TrackStart();

            var exceptions = new List<Exception>();
            var channelPublisher = EventLoggingFactory.GetNewWorkItemQueuePublisher();

            foreach (EventData eventData in events)
            {
                try
                {
                    // parse message
                    string messageBody = Encoding.UTF8.GetString(eventData.Body.Array, eventData.Body.Offset, eventData.Body.Count);
                    var eventLog = JsonConvert.DeserializeObject<LogMessage>(messageBody);

                    // is this a work item?
                    if (eventLog.ShouldPublishAsWorkItem())
                    {
                        // append service path info
                        eventLog.MessagePath += $" -> {context.FunctionName}";
                        eventLog.UpdatedDateTime = DateTime.Now;

                        // publish event
                        await channelPublisher.PublishAsync(eventLog);
                    }

                    log.LogInformation($"C# Event Hub trigger function processed a message: {messageBody}");
                    await Task.Yield();
                }
                catch (Exception e)
                {
                    // We need to keep processing the rest of the batch - capture this exception and continue.
                    // Also, consider capturing details of the message that failed processing so it can be processed again later.
                    exceptions.Add(e);
                }
            }

            // Once processing of the batch is complete, if any messages in the batch failed processing throw an exception so that there is a record of the failure.

            if (exceptions.Count > 1)
                throw new AggregateException(exceptions);

            if (exceptions.Count == 1)
                throw exceptions.Single();

            FunctionContextHelper.TrackEnd();
        }
    }
}
