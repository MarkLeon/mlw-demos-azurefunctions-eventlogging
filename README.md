# MLW.Demos.Azure.Functions.EventLogging

## About

This repo contains sample code used during a presentation and demonstration of Azure Functions.

## Use Case: Integrated Event Logging and Triggered Notifications

Publish events that could be used to inform and/or trigger other systems.

Use a familiar logging paradigm with log levels (DEBUG, INFO, WARN, ERROR, FATAL).

### Requirements

- Accept log events posted to an HTTP Endpoint using an Azure Function HTTP Trigger.
- Parse log events from HTTP poster and publish to an Azure Event Hub.
- Process log events from Event Hub using an Azure Function Event Hub Trigger. 
- Publish all log events to a Slack channel for review and ad-hoc querying.
- Publish error log events to a Service Bus Queue for processing.
- Process error events from Service Bus Queue using an Azure Function Service Bus Queue Trigger. 
- Publish error log events to a Teams channel for review.

## Running/Debugging

This demo uses Azure resources and will require an Azure subscription.

This demo also used Slack resources and will require and Slack account with 2 channel webhooks.

This demo will also require that the Azure resources be created and connection strings updated in the `local.settings.json` file.

Once those requirements have been completed, this demo can be run inside Visual Studio.

## Diagrams

- [High Level Diagram (draw.io)](docs/MLW.Demos.Azure.Functions.EventLogging-Diagram-draw.io.jpg)

![](./docs/MLW.Demos.Azure.Functions.EventLogging-Diagram-draw.io.jpg)

## Resources

- [Azure Functions Overview | Microsoft Docs](https://docs.microsoft.com/en-us/azure/azure-functions/functions-overview)

## Release Notes

v0.1.0 - Initial commit

v1.0.0 - FWDNUG presentation

